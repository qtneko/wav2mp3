# Wav2Mp3
# Build
## Requirements
* GCC/G++ 8.1 or later.
* CMake version 3.12 or later.
* libmp3lame (Lame).
* Make.
## Windows
* Install msys2 and launch mingw64.
* Install: GCC, CMake, Make and Lame:
```bash
pacman -Syu
pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-lame mingw-w64-x86_64-cmake mingw-w64-x86_64-make
```
```bash
cd /path/to/Wav2Mp3
```
* Build application
```bash
mkdir tmp
cd tmp
cmake -G"Unix Makefiles" ../
cmake --build .
```
* Binary wav2mp3.exe will be built to `/path/to/Wav2Mp3/bin`
## Linux
* Install GCC/G++ 8, CMake, Make and libmp3lame-dev. 
* Build application
```bash
cd /path/to/Wav2Mp3
mkdir tmp
cd tmp
cmake -G"Unix Makefiles" ../
cmake --build .
```
* Binary wav2mp3 will be built to `/path/to/Wav2Mp3/bin`
# Usage
```bash
./wav2mp3.exe <path>
```
# Note
Project can be built without POSIX threads. One should use
```bash
cmake -G"Unix Makefiles" -DUSE_PTHREAD=OFF ../
```