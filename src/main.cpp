#include <iostream>
#include "WavToMp3.h"

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " <path>\n"; 
		return 1;
	}
	WavToMp3::encodeAll(argv[1]);
	return 0;
}
