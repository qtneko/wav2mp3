#pragma once

#include <string>
#include <list>

namespace Utils
{
	std::list<std::string> getFileListByExt(const std::string &path,
		const std::string &ext, std::string &errorMsg);
};

