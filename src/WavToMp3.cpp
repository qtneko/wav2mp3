#include "WavToMp3.h"

#include <iostream>
#include <vector>
#include <string>
#include <lame/lame.h>

#include "Utils.h"
#include "WavReader.h"
#include "Timer.h"

#ifdef USE_PTHREAD
#include "PosixConcurrent.h"
#else
#include "Concurrent.h"
#endif

static const std::string WAV_EXT = ".wav";
static const std::string MP3_EXT = ".mp3";

const size_t PCM_SIZE = 1u << 14;
const size_t MP3_SIZE = 1u << 15;

void WavToMp3::encodeAll(const std::string &path)
{
	std::string errorMsg;
	const auto fileList = Utils::getFileListByExt(path, WAV_EXT, errorMsg);
	if (!errorMsg.empty())
	{
		std::cerr << errorMsg << std::endl;
		return;
	}

	if (fileList.empty())
	{
		std::cout << "There are no \"" << WAV_EXT << "\" files in " << path << std::endl;
	}
	else
	{
		encode(fileList);
		std::cout << "Done!\n";
	}
}

void WavToMp3::encode(const std::list<std::string> &fileList)
{
	Timer timer;
	timer.start();
	bool (*encode)(const std::string &) = &WavToMp3::encode;
#ifdef USE_PTHREAD
	PosixConcurrent::blockingMap(fileList.begin(), fileList.end(), encode);
#else
	Concurrent::blockingMap(fileList.begin(), fileList.end(), encode);
#endif
	timer.stop();
	std::cout << "Elapsed time: " << timer.elapsedTime() << " ms.\n";
}

bool WavToMp3::encode(const std::string &wavFileName)
{
	const std::string title = "Encoding:" + wavFileName;
	auto printError = [&title](const std::string &msg)
	{
		std::cerr << title + '\n' + msg + '\n';
	};
	WavReader wavReader;
	if (!wavReader.open(wavFileName))
	{
		printError(wavReader.lastError());
		return false;
	}

	const std::string mp3FileName = replaceExt(wavFileName, MP3_EXT);
	std::ofstream mp3Stream(mp3FileName, std::ios::binary);
	if (!mp3Stream.is_open())
	{
		printError("Unable to open:" + mp3FileName);
		return false;
	}

	lame_t lame = lame_init();

	auto handleError = [lame, &printError](const std::string &sv)
	{
		printError(sv);
		lame_close(lame);
	};

	if (lame == nullptr)
	{
		handleError("Lame init error!");
		return false;
	}

	lame_set_in_samplerate(lame, static_cast<int>(wavReader.sampleRate()));
	lame_set_num_channels(lame, static_cast<int>(wavReader.numberOfChannels()));
	const int goodQuality = 5;
	lame_set_quality(lame, goodQuality);
	if (lame_init_params(lame) < 0)
	{
		handleError("Lame init error!");
		return false;
	}

	std::vector<int16_t> pcmBuf;
	std::vector<uint8_t> mp3Buf(MP3_SIZE);

	bool done = false;
	while (!done)
	{
		const size_t samplesRead = wavReader.read(pcmBuf, PCM_SIZE);
		done = (samplesRead == 0);
		const int result = done ?
			lame_encode_flush(lame, mp3Buf.data(), MP3_SIZE) :
			lame_encode_buffer_interleaved(lame, pcmBuf.data(), samplesRead, mp3Buf.data(), MP3_SIZE);
		switch (result)
		{
		case 0:
			break;
		case -1:
			handleError("Lame error: mp3buf was too small!");
			return false;
		case -2:
			handleError("Lame error: malloc() problem!");
			return false;
		case -3:
			handleError("Lame error: lame_init_params() not called!");
			return false;
		case -4:
			handleError("Lame error: psycho acoustic problems!");
			return false;
		default:
			if (!mp3Stream.write(reinterpret_cast<const char *>(mp3Buf.data()), result))
			{
				handleError("An error occurred when attempting to write data to file!");
				return false;
			}
			break;
		}
	}
	if (const std::string lastError = wavReader.lastError(); !lastError.empty())
	{
		handleError(lastError);
		return false;
	}
	lame_close(lame);

	std::cout << title + " Done!\n";
	return true;
}

std::string WavToMp3::replaceExt(const std::string &fileName, const std::string &newExt)
{
	if (const size_t lastIndex = fileName.find_last_of('.'); lastIndex != std::string::npos)
	{
		return fileName.substr(0, lastIndex) + newExt;
	}
	else
	{
		return fileName + newExt;
	}
}
