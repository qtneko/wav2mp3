#pragma once
#include <pthread.h>
#include <thread>
#include <iostream>
#include <algorithm>
#include <vector>

class PosixConcurrent
{
public:
	PosixConcurrent() = delete;

	//Calls function once for each item in sequence.
	template<typename It, typename Func>
	static bool blockingMap(It beg, It end, Func func)
	{
		const unsigned int maxThreadsCount =
			std::max(1u, std::thread::hardware_concurrency());
		std::vector<pthread_t> pool;
		Helper<It, Func> helper;
		helper.func = func;
		helper.curIt = beg;
		helper.endIt = end;
		using LambdaType = void*(*)(void*);
		LambdaType process = [](void *arg)->void*
		{
			auto helper = static_cast<Helper<It, Func> *>(arg);
			while (true)
			{
				pthread_mutex_lock(&helper->mutex);
				if (helper->curIt == helper->endIt)
				{
					pthread_mutex_unlock(&helper->mutex);
					return nullptr;
				}
				else
				{
					const auto &item = *helper->curIt;
					++helper->curIt;
					pthread_mutex_unlock(&helper->mutex);
					helper->func(item);
				}
			}
			return nullptr;
		};
		bool ret = true;
		for (unsigned int i = 0; i < maxThreadsCount; ++i)
		{
			pthread_t thread;
			const int res = pthread_create(&thread, nullptr,
								process, &helper);
			switch (res)
			{
			case 0:
				pool.emplace_back(thread);
				break;
			default:
				ret = false;
				std::cerr << "Unable to create POSIX thread. Error: " << res << std::endl;
				break;
			}
		}
		for (auto &thread : pool)
		{
			pthread_join(thread, nullptr);
		}
		return ret;
	}

private:

	template<typename It, typename Func>
	struct Helper
	{
		pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
		Func			func;
		It				curIt;
		It				endIt;
	};

};
