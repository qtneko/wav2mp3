#include "Utils.h"

#include <filesystem>
#include <algorithm>

std::list<std::string> Utils::getFileListByExt(const std::string &path,
	const std::string &ext, std::string &errorMsg)
{
	namespace fs = std::filesystem;

	std::list<std::string> list;

	const fs::path dir(path);
	std::error_code errorCode;
	if (!fs::exists(dir, errorCode))
	{
		errorMsg = "Path " + dir.string() + " does not exist.";
		return list;
	}

	auto toLowerCase = [](const std::string &str)
	{
		std::string res(str);
		std::transform(res.begin(), res.end(), res.begin(), tolower);
		return res;
	};
	const std::string extension = toLowerCase(ext);

	for (const fs::directory_entry &entry : fs::directory_iterator(dir))
	{
		if (entry.is_regular_file())
		{
			const fs::path path = entry.path();
			const std::string curExt = toLowerCase(path.extension().string());
			if (curExt == ext)
			{
				list.emplace_back(path.string());
			}
		}
	}
	return list;
}
