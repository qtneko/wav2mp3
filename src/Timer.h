#pragma once
#include <chrono>
#include <ctime>

class Timer
{
public:
	void start()
	{
		_start = std::chrono::system_clock::now();
	}

	void stop()
	{
		_stop = std::chrono::system_clock::now();
	}

	template<typename Duration = std::chrono::milliseconds>
	int elapsedTime() const
	{
		return std::chrono::duration_cast<Duration>(_stop - _start).count();
	}

private:
	std::chrono::time_point<std::chrono::system_clock>  _start;
	std::chrono::time_point<std::chrono::system_clock>  _stop;
};