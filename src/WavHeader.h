#pragma once
#include <cstdint>

#pragma pack(push,1)
struct WavHeader
{
	/* RIFF Chunk Descriptor */
	uint32_t chunkId;
	uint32_t chunkSize;
	uint32_t format;
	/* "fmt" sub-chunk */
	uint32_t subchunk1Id;
	uint32_t subchunk1Size;
	uint16_t audioFormat;
	uint16_t numChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
	uint16_t bitsPerSample;
	/* "data" sub-chunk */
	uint32_t subchunk2Id;
	uint32_t subchunk2Size;
};
#pragma pack(pop)