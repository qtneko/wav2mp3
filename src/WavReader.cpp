#include "WavReader.h"
#include "WavHeader.h"

WavReader::WavReader(const std::string &fileName)
	: _fileName(fileName)
{
}

WavReader::~WavReader()
{
	close();
}

bool WavReader::open()
{
	if (isOpen())
	{
		_lastError = _fileName + " is already opened!";
		return false;
	}
	_fileStream.open(_fileName, std::ios_base::binary);
	if (!isOpen())
	{
		_lastError = "Failed to open " + _fileName;
		return false;
	}
	if (!readHeader())
	{
		return false;
	}
	return true;
}

bool WavReader::open(const std::string &fileName)
{
	_fileName = fileName;
	return open();
}

bool WavReader::isOpen() const
{
	return _fileStream.is_open();
}

void WavReader::close()
{
	if (isOpen())
	{
		_fileStream.close();
	}
}

size_t WavReader::read(std::vector<int16_t> &buf, size_t samplesPerChannelToRead)
{
	if (!isOpen())
	{
		_lastError = "Trying to read unopened file!";
		return 0;
	}
	if (_fileStream.eof())
	{
		buf.clear();
		return 0;
	}
	const size_t samplesToRead = samplesPerChannelToRead * _header.numChannels;
	const size_t bytesPerSample = _header.bitsPerSample / 8;
	const size_t bytesToRead = samplesToRead * bytesPerSample;
	buf.resize(samplesToRead);
	_fileStream.read(reinterpret_cast<char*>(buf.data()), bytesToRead);
	if (!_fileStream.eof() && _fileStream.fail())
	{
		_lastError = "Read error!";
		buf.clear();
		return 0;
	}
	const size_t readSamples = static_cast<size_t>(_fileStream.gcount()) / bytesPerSample;
	buf.resize(readSamples);
	const size_t readSamplesPerChannel = readSamples / _header.numChannels;
	return readSamplesPerChannel;
}

unsigned int WavReader::sampleRate() const
{
	return _header.sampleRate;
}

unsigned short WavReader::bitsPerSample() const
{
	return _header.bitsPerSample;
}

unsigned short WavReader::numberOfChannels() const
{
	return _header.numChannels;
}

std::string WavReader::lastError() const
{
	return _lastError;
}

bool WavReader::readHeader()
{
	if (!_fileStream.read(reinterpret_cast<char *>(&_header), sizeof(_header)))
	{
		return false;
	}
	if (_header.chunkId != 0x46464952) //"RIFF"
	{
		_lastError = "Wav header error!";
		return false;
	}
	if (_header.format != 0x45564157) //"WAVE"
	{
		_lastError = "Wav header error!";
		return false;
	}
	if (_header.subchunk1Id != 0x20746D66) //"fmt "
	{
		_lastError = "Wav header error!";
		return false;
	}
	if (_header.subchunk2Id != 0x61746164) //"data"
	{
		_lastError = "Wav header error!";
		return false;
	}
	if (_header.audioFormat != 1) //PCM
	{
		_lastError = "Unsupported audio format!";
		return false;
	}
	if (_header.bitsPerSample != 16)
	{
		_lastError = "Unsupported BPS format!";
		return false;
	}
	if (_header.numChannels != 2)
	{
		_lastError = "Unsupported number of channels!";
		return false;
	}
	return true;
}
