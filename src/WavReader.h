#pragma once
#include <string>
#include <fstream>
#include <vector>

#include "WavHeader.h"

class WavReader
{
public:
	WavReader() = default;
	WavReader(const std::string &fileName);
	~WavReader();

	bool open();
	bool open(const std::string &fileName);
	bool isOpen() const;
	void close();

	// returns exact count of read samples per channel
	size_t read(std::vector<int16_t> &buf, size_t samplesPerChannelToRead);

	unsigned int sampleRate() const;
	unsigned short bitsPerSample() const;
	unsigned short numberOfChannels() const;

	std::string lastError() const;

private:
	std::string					_fileName;
	std::ifstream				_fileStream;
	WavHeader					_header;
	std::string					_lastError;

	bool readHeader();

};

