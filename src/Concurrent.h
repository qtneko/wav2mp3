#pragma once
#include <thread>
#include <mutex>
#include <algorithm>
#include <vector>

class Concurrent
{
public:
	Concurrent() = delete;
	//Calls function once for each item in sequence.
	template<typename It, typename Func>
	static void blockingMap(It beg, It end, Func func)
	{
		void(*process)(std::mutex &, It &, It, Func) = &Concurrent::process;
		const unsigned int maxThreadsCount =
			std::max(1u, std::thread::hardware_concurrency());
		std::vector<std::thread> pool;
		std::mutex mutex;
		for (unsigned int i = 0; i < maxThreadsCount; ++i)
		{
			pool.emplace_back(process, std::ref(mutex), std::ref(beg), end, func);
		}
		for (auto &thread : pool)
		{
			thread.join();
		}
	}

private:
	template<typename It, typename Func>
	static void process(std::mutex &mutex, It &it, It end, Func func)
	{
		while (true)
		{
			mutex.lock();
			if (it == end)
			{
				mutex.unlock();
				return;
			}
			const auto &item = *it;
			++it;
			mutex.unlock();
			func(item);
		}
	}
};
