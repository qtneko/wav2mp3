#pragma once
#include <string>
#include <list>

class WavToMp3
{
public:
	WavToMp3() = delete;

	static void encodeAll(const std::string &path);
	static void encode(const std::list<std::string> &fileList);
	static bool encode(const std::string &wavFileName);

private:
	static std::string replaceExt(const std::string &fileName,
		const std::string &newExt);
};

